# Flutter Hello World Project

## 1. Project Description

This is a simple Hello World project using Flutter. It serves as a starting point for Flutter development and helps you verify that your Flutter environment is set up correctly.

## 2. Prerequisites

Before you begin, ensure you have met the following requirements:

- [Flutter](https://flutter.dev/docs/get-started/install) installed
- [Dart SDK](https://dart.dev/get-dart) installed
- A code editor such as [Visual Studio Code](https://code.visualstudio.com/) or [Android Studio](https://developer.android.com/studio) installed

## 3. Getting Started

### Clone the Repository

```
git clone https://gitlab.com/techtalk5782345/flutter_hello_world
```
```
cd flutter-hello-world
```
## Run the Flutter App
```
flutter run
```

## Project Structure
- ``/lib/main.dart`` : Contains the main.dart file which is the entry point of the flutter app